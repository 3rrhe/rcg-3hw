import React, { Component } from 'react';

class Course extends Component {
    state = {
        activeCourse: null
    };

    componentDidMount() {
        this.loadData();
    }

    componentDidUpdate(nextProps, nextState, nextContext) {
        this.loadData();
    }

    loadData = () => {
        if ( !this.state.activeCourse || (this.state.activeCourse.id !== this.props.match.params.id)) {
            const query = new URLSearchParams(this.props.location.search);

            let title = null;

            for (let param of query.entries()) {
                title = param[1];
            }

            this.setState({activeCourse: {
                    id: this.props.match.params.id,
                    title: title
                } });
        }
    };

    render () {
        return (
            <div>
                { this.state.activeCourse ?
                    <div>
                        <h1>{this.state.activeCourse.title}</h1>
                        <p>You selected the Course with ID: {this.props.match.params.id}</p>
                    </div>
                 : null }
            </div>
        );
    }
}

export default Course;